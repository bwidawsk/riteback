//! All operations relating to connectors

use drm::control::{crtc, Device};

use anyhow::Context;
use drm::control::connector;

use crate::{mode, Mode};

use super::{drm_crtc::get_prop_crtc, InnerDevice, WbConnector};

/// Get a name for the connector
pub const fn connector_name(interface: connector::Interface) -> &'static str {
    match interface {
        connector::Interface::DisplayPort => "DP",
        connector::Interface::HDMIA => "HDMI",
        connector::Interface::HDMIB => "HDMI",
        connector::Interface::EmbeddedDisplayPort => "eDP",
        connector::Interface::DSI => "DSI",
        connector::Interface::Virtual => "VKMS",
        connector::Interface::Writeback => "Writeback",
        _ => todo!(),
    }
}

impl InnerDevice {
    pub fn crtc_id(&self, connector: &connector::Info) -> Option<crtc::Handle> {
        get_prop_crtc(self, connector.handle(), "CRTC_ID")
    }

    pub fn connectors<'a>(&'a self) -> impl Iterator<Item = &'a connector::Info> {
        self.crtcs_for_connector.keys().into_iter()
    }

    pub fn active_connectors<'a>(&'a self) -> impl Iterator<Item = &'a connector::Info> {
        self.crtcs_for_connector
            .keys()
            .into_iter()
            .filter(|connector| {
                connector.state() == connector::State::Connected
                    && get_prop_crtc(self, connector.handle(), "CRTC_ID").is_some()
            })
    }
    pub fn connector_from_name<'a>(
        &'a self,
        connector: &WbConnector,
    ) -> Option<&'a connector::Info> {
        self.crtcs_for_connector
            .keys()
            .into_iter()
            .find(|conn| WbConnector::from(*conn) == *connector)
    }

    pub fn is_writeback(&self, connector: &WbConnector) -> bool {
        self.connector_from_name(connector)
            .is_some_and(|c| c.interface() == connector::Interface::Writeback)
    }

    /// Assumes connector is connected to a CRTC and active.
    pub fn connector_mode(&self, connector: &connector::Info) -> anyhow::Result<Mode> {
        let crtc = self
            .crtc_id(connector)
            .with_context(|| format!("No CRTC for {:?}", connector))?;
        let crtc_info = self.get_crtc(crtc)?;
        let drm_mode = crtc_info.mode().with_context(|| "No CRTC info")?;
        Ok(mode(&drm_mode))
    }

    pub fn writeback_connectors<'a>(
        &'a self,
        free: bool,
    ) -> impl Iterator<Item = &'a connector::Info> {
        self.crtcs_for_connector
            .keys()
            .into_iter()
            .filter(|connector| connector.interface() == connector::Interface::Writeback)
            .filter(move |connector| {
                !free
                    || connector.current_encoder().is_none()
                        && get_prop_crtc(self, connector.handle(), "CRTC_ID").is_none()
            })
    }
}
