use std::rc::{Rc};

use anyhow::bail;
use drm::control::connector;

use crate::{hz, Mode};

use super::InnerDevice;

#[allow(unused)]
#[derive(Debug)]
pub struct WbConnectorDev {
    dev: Rc<InnerDevice>,
    handle: connector::Handle,
    /*
     formats: Vec<DrmFourcc>,
    format: DrmFourcc,
    framebuffer: framebuffer::Handle,
    target: crtc::Handle,
    */
}

impl WbConnectorDev {
    /*
    pub fn new(
        wbd: &WbDevice,
        conn: &connector::Info,
    ) -> anyhow::Result<Self> {
        let format_blob = get_prop_blob(wbd, conn.handle(), "WRITEBACK_PIXEL_FORMATS")
            .context("No WRITE_PIXEL_FORMATS prop found")?;

        let mut formats = Vec::new();
        let _ = wbd.get_property_blob(format_blob).map(|bytes| {
            bytes.chunks_exact(4).for_each(|chunk| {
                let dword = u32::from_ne_bytes(chunk.try_into().unwrap());
                if let Ok(f) = DrmFourcc::try_from(dword) {
                    formats.push(f);
                }
            })
        });

        // The mode of the CRTC we're trying to capture.
        let mode: Mode = wbd
            .get_crtc(*target)
            .map(|ref crtc_info| crtc_info.mode())
            .transpose()
            .with_context(|| format!("No crtc for {:?}", target))?
            .map(|ref crtc_mode| mode(crtc_mode))?;

        let f = formats.first().unwrap();
        let dumb = wbd.create_dumb_buffer(mode.size, *f, bpp(f)).unwrap();
        let fb = wbd
            .add_framebuffer(&dumb, depth(f), bpp(f))
            .with_context(|| "Failed to create framebuffer")?;

        Ok(Self {
            handle: conn.handle(),
            format: *f,
            framebuffer: fb,
            target: *target,
        })
    }
    */
    pub fn find_writeback_connector(dev: Rc<InnerDevice>, mode: Mode) -> anyhow::Result<Self> {
        for c in dev.writeback_connectors(true) {
            let Some(_mode) = c
                .modes()
                .iter()
                .filter(|cm| cm.size() == mode.to_u16())
                .find(|cm| hz(cm) - mode.refresh < 0.01f64) else {
                continue
            };

            return Ok(Self {
                dev: Rc::clone(&dev),
                handle: c.handle(),
            })
        }
        bail!("No valid crtc found for connector");
    }
        /*
                    if let Some(mode) = connector
                        .1
                        .modes()
                        .iter()
                        .filter(|m| m.size().0 as i32 == mode.0 && m.size().1 as i32 == mode.1)
                        .reduce(|a, b| {
                            if (hz(a) - current_refresh).abs() < (hz(b) - current_refresh).abs() {
                                a
                            } else {
                                b
                            }
                        })
                    {
                        return Ok(*crtc);
                    } else {
                        failed_crtcs.insert(*crtc);
                    }
                }

                debug_assert_eq!(all_crtcs, failed_crtcs);
        */
}
