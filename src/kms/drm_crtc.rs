use drm::control::{self, crtc, property, ResourceHandle};

use super::{get_property_val, AtomicKmsWbDevice};

pub fn get_prop_crtc(
    device: &(impl control::Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> Option<crtc::Handle> {
    match get_property_val(&device, handle, name) {
        Ok((_0, _1, _2)) => match _1.convert_value(_2) {
            property::Value::CRTC(a) => a,
            _ => unreachable!(),
        },
        Err(_e) => None,
    }
}

impl AtomicKmsWbDevice {
    /*
    fn find_crtc(
        &self,
        mode: (i32, i32, i32),
        connector: (&connector::Handle, &connector::Info),
    ) -> anyhow::Result<crtc::Handle> {
        // Find a good CRTC for the added connector
        let all_crtcs = self.possible_crtcs_for_connector(connector.1, true);
        let mut failed_crtcs: HashSet<crtc::Handle> = HashSet::new();
        for crtc in &all_crtcs {
            let hz = |m: &control::Mode| -> f64 {
                m.clock() as f64 * 1000.0 / (m.hsync().2 as f64 * m.vsync().2 as f64)
            };
            let current_refresh: f64 = f64::from(mode.2);
            // This will find the *actual* FPS that's closest to the requested for the given size
            if let Some(mode) = connector
                .1
                .modes()
                .iter()
                .filter(|m| m.size().0 as i32 == mode.0 && m.size().1 as i32 == mode.1)
                .reduce(|a, b| {
                    if (hz(a) - current_refresh).abs() < (hz(b) - current_refresh).abs() {
                        a
                    } else {
                        b
                    }
                })
            {
                return Ok(*crtc);
            } else {
                failed_crtcs.insert(*crtc);
            }
        }

        debug_assert_eq!(all_crtcs, failed_crtcs);
        bail!("No valid crtc found for connector {:?}", connector.0);
    }
    */
}
