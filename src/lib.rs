use drm::control;

pub mod kms;

pub struct Mode {
    pub size: (u32, u32),
    pub refresh: f64,
}

impl std::fmt::Display for Mode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}x{}@{}hz", self.size.0, self.size.1, self.refresh)
    }
}

pub fn hz(drm_mode: &control::Mode) -> f64 {
    let hz = |m: &control::Mode| -> f64 {
        m.clock() as f64 * 1000.0 / (m.hsync().2 as f64 * m.vsync().2 as f64)
    };
    hz(&drm_mode)
}

pub fn mode(drm_mode: &control::Mode) -> Mode {
    Mode {
        size: (drm_mode.size().0 as u32, drm_mode.size().1 as u32),
        refresh: hz(&drm_mode),
    }
}

impl Mode {
    fn to_u16(&self) -> (u16, u16) {
        (self.size.0 as u16, self.size.1 as u16)
    }
}
