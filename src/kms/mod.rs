pub mod drm_connector;
pub mod drm_crtc;
mod drm_framebuffers;
mod drm_planes;
mod formats;
mod surface;
pub mod wb_connector;

use std::{
    collections::{HashMap, HashSet},
    fs::File,
    os::{
        fd::{AsFd, OwnedFd},
        unix::prelude::BorrowedFd,
    },
    rc::Rc,
    str::FromStr,
};

use anyhow::Context;
use drm::control::{self, connector, crtc, property, ResourceHandle};
use drm::control::{plane, Device as ControlDevice};
use drm::Device as BasicDevice;
use tracing::warn;

use self::drm_connector::connector_name;

pub struct ParseConnectorErr;

impl std::fmt::Display for ParseConnectorErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Failed to parse connector")
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct WbConnector(String, usize);

impl std::fmt::Display for WbConnector {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-{}", self.0, self.1)
    }
}

impl FromStr for WbConnector {
    type Err = ParseConnectorErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (name, id) = s.split_once('-').ok_or(ParseConnectorErr)?;
        Ok(WbConnector(
            name.to_string(),
            usize::from_str(id).map_err(|_e| ParseConnectorErr)?,
        ))
    }
}

impl From<(&str, usize)> for WbConnector {
    fn from(value: (&str, usize)) -> Self {
        WbConnector(value.0.to_string(), value.1)
    }
}

impl From<&connector::Info> for WbConnector {
    fn from(value: &connector::Info) -> Self {
        WbConnector(
            connector_name(value.interface()).to_string(),
            value.interface_id() as usize,
        )
    }
}

/// Get a raw property value from KMS.
///
/// <https://www.kernel.org/doc/html/latest/gpu/drm-kms.html?highlight=properties#existing-kms-properties>
///
/// # Errors
///
/// This function will return an error if either the device properties, or the `name` cannot be
/// found.
#[allow(clippy::just_underscores_and_digits)]
fn get_property_val(
    device: &&(impl control::Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> anyhow::Result<(property::Handle, property::ValueType, property::RawValue)> {
    let props = device
        .get_properties(handle)
        .with_context(|| format!("Property error for {device:?}"))?;
    let (prop_handles, values) = props.as_props_and_values();
    for (&prop, &val) in prop_handles.iter().zip(values.iter()) {
        let info = device
            .get_property(prop)
            .with_context(|| format!("Property {name}"))?;
        if Some(name) == info.name().to_str().ok() {
            let val_type = info.value_type();
            return Ok((prop, val_type, val));
        }
    }
    anyhow::bail!("No prop found {name}")
}

// Get a boolean property from KMS.
#[allow(clippy::just_underscores_and_digits, unused)]
fn get_prop_bool(
    device: &(impl control::Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> bool {
    match get_property_val(&device, handle, name) {
        Ok((_0, _1, _2)) => match _1.convert_value(_2) {
            property::Value::Boolean(a) => a,
            _ => unreachable!(),
        },
        Err(_e) => false,
    }
}

/// Get a blob property from KMS.
#[allow(clippy::just_underscores_and_digits, unused)]
pub fn get_prop_blob(
    device: &(impl control::Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> Option<u64> {
    match get_property_val(&device, handle, name) {
        Ok((_0, _1, _2)) => match _1.convert_value(_2) {
            property::Value::Blob(a) => Some(a),
            _ => unreachable!(),
        },
        Err(_e) => None,
    }
}

pub type Mapping = (
    HashMap<connector::Handle, HashMap<String, property::Handle>>,
    HashMap<crtc::Handle, HashMap<String, property::Handle>>,
    HashMap<plane::Handle, HashMap<String, property::Handle>>,
);

fn props<T: ResourceHandle + Eq + std::hash::Hash>(
    device: &(impl control::Device + std::fmt::Debug),
    handles: &[T],
) -> anyhow::Result<HashMap<T, HashMap<String, property::Handle>>> {
    let mut ret = HashMap::new();
    handles
        .iter()
        .map(|x| (x, device.get_properties(*x)))
        .try_for_each(|(handle, props)| {
            let mut map = HashMap::new();
            match props {
                Ok(props) => {
                    let (prop_handles, _) = props.as_props_and_values();
                    for prop in prop_handles {
                        if let Ok(info) = device.get_property(*prop) {
                            let name = info.name().to_string_lossy().into_owned();
                            map.insert(name, *prop);
                        }
                    }
                    ret.insert(*handle, map);
                    Ok(())
                }
                Err(err) => Err(err),
            }
        })?;
    Ok(ret)
}

type ConnectorCrtc = HashMap<connector::Info, Vec<crtc::Info>>;

#[derive(Debug)]
pub struct DeviceBuilder {
    fd: OwnedFd,
    properties: Option<Mapping>,
    crtcs_for_connector: Option<ConnectorCrtc>,
    planes: Option<Vec<plane::Info>>,
}

#[derive(Debug)]
pub struct InnerDevice {
    fd: OwnedFd,
    properties: Mapping,
    crtcs_for_connector: ConnectorCrtc,
}

#[derive(Debug)]
pub struct AtomicKmsWbDevice {
    inner: Rc<InnerDevice>,
}

impl std::ops::Deref for AtomicKmsWbDevice {
    type Target = Rc<InnerDevice>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl TryFrom<&OwnedFd> for AtomicKmsWbDevice {
    type Error = anyhow::Error;

    fn try_from(value: &OwnedFd) -> Result<Self, Self::Error> {
        let fd = value.try_clone()?;
        Ok(DeviceBuilder::new(fd.into())
            .populate_props()?
            .with_caps(
                [
                    drm::ClientCapability::UniversalPlanes,
                    drm::ClientCapability::Atomic,
                    drm::ClientCapability::WritebackConnectors,
                ]
                .as_slice(),
            )?
            .populate_objects()?
            .build())
    }
}

impl DeviceBuilder {
    fn new(f: File) -> Self {
        Self {
            fd: f.into(),
            properties: None,
            crtcs_for_connector: None,
            planes: None,
        }
    }

    fn populate_props(mut self) -> anyhow::Result<Self> {
        let res_handles = self.resource_handles()?;
        let conns = props(&self, res_handles.connectors())?;
        let crtcs = props(&self, res_handles.crtcs())?;
        let planes = props(&self, &self.plane_handles()?)?;
        self.properties.replace((conns, crtcs, planes));
        Ok(self)
    }

    fn with_caps(self, caps: &[drm::ClientCapability]) -> anyhow::Result<Self> {
        for cap in caps.iter() {
            self.set_client_capability(*cap, true)?;
        }
        Ok(self)
    }

    fn connectors(device: &(impl control::Device + std::fmt::Debug)) -> HashSet<connector::Info> {
        let mut connectors: HashSet<connector::Info> = HashSet::new();

        match device.resource_handles() {
            Ok(rhandles) => {
                rhandles
                    .connectors()
                    .iter()
                    .for_each(|h| match device.get_connector(*h, false) {
                        Ok(info) => {
                            connectors.insert(info);
                        }
                        Err(e) => warn!("{e}"),
                    })
            }
            Err(e) => {
                eprintln!("{e}");
            }
        };

        connectors
    }

    fn populate_objects(mut self) -> anyhow::Result<Self> {
        let mut mapping = HashMap::new();
        for connector in Self::connectors(&self) {
            let possible_crtcs = Vec::from_iter(
                connector
                    .encoders()
                    .iter()
                    .flat_map(|eh| self.get_encoder(*eh))
                    .flat_map(|encoder_info| match self.resource_handles() {
                        Ok(handles) => handles.filter_crtcs(encoder_info.possible_crtcs()),
                        Err(e) => {
                            eprintln!("{e}");
                            vec![]
                        }
                    })
                    .flat_map(|crtc_handle| self.get_crtc(crtc_handle)),
            );
            mapping.insert(connector, possible_crtcs);
        }
        self.crtcs_for_connector.replace(mapping);

        self.planes.replace(drm_planes::planes(&self));
        Ok(self)
    }

    fn build(self) -> AtomicKmsWbDevice {
        let inner = InnerDevice {
            fd: self.fd,
            properties: self.properties.unwrap(),
            crtcs_for_connector: self.crtcs_for_connector.unwrap(),
        };
        AtomicKmsWbDevice {
            inner: Rc::new(inner),
        }
    }
}

impl AtomicKmsWbDevice {
    pub fn new(f: File) -> anyhow::Result<Self> {
        Ok(DeviceBuilder::new(f.into())
            .populate_props()?
            .with_caps(
                [
                    drm::ClientCapability::UniversalPlanes,
                    drm::ClientCapability::Atomic,
                    drm::ClientCapability::WritebackConnectors,
                ]
                .as_slice(),
            )?
            .populate_objects()?
            .build())
    }

    fn crtcs_for_connector(
        &self,
        connector: &connector::Info,
    ) -> impl Iterator<Item = &crtc::Info> {
        let Some(crtcs) = self.crtcs_for_connector.get(connector) else {
            return [].iter();
        };

        crtcs.iter()
    }

    /// Get all possible CRTCs for a connector
    #[allow(unused)]
    fn possible_crtcs_for_connector<'a>(
        &'a self,
        connector: &connector::Info,
        active: bool,
    ) -> impl Iterator<Item = &crtc::Info> + 'a {
        self.crtcs_for_connector(connector).filter(move |ref crtc| {
            !active || get_prop_bool(&*self.inner, (*crtc).handle(), "ACTIVE")
        })
    }
}

impl ControlDevice for InnerDevice {}
impl ControlDevice for DeviceBuilder {}

impl BasicDevice for InnerDevice {}
impl BasicDevice for DeviceBuilder {}

impl AsFd for InnerDevice {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.fd.as_fd()
    }
}

impl AsFd for DeviceBuilder {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.fd.as_fd()
    }
}
