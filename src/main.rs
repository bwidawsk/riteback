use std::fs::File;

use anyhow::Context;
use bpaf::{Bpaf, Parser};

use drm::{control::connector, Device as BasicDevice};

use riteback::kms::{AtomicKmsWbDevice, WbConnector};

#[derive(Debug, Bpaf)]
struct Args {
    /// The card node to try to screenshot from.
    #[bpaf(
        short('n'),
        long,
        argument("/dev/dri/cardX"),
        fallback(1),
        display_fallback
    )]
    node: usize,

    /// List connectors and exit
    #[bpaf(short('l'), long)]
    list_connectors: bool,

    /// List of connectors that should be screenshotted.
    #[bpaf(short, long)]
    connectors: Vec<WbConnector>,
}

fn do_screenshot(dev: &AtomicKmsWbDevice, connectors: &[&connector::Info]) {
    /*
        let wb_conns = wbd.writeback_connectors(true);
        if wb_conns.len() > 1 {
            panic!("Can't handle");
        }
        let wbc: WbConnectorDev = wb_conns
            .into_iter()
            .next()
            .context("No Writeback connector found")
            .and_then(|ref info| WbConnectorDev::new(&wbd, info))
            .context("Couldn't create a writeback connector")?;
    */
    for c in connectors {
        let _target_mode = dev.connector_mode(c);
        let Some(_target_crtc) = dev.crtc_id(c) else {
            continue;
        };
        //        Writeback::new(wbd, &conn, &target_crtc);
        //        connector_mode(wbd, &conn).unwrap());
        //wbd.create_dumb_buffer(, format, bpp)
    }
    /*
       get_prop_blob(wbd, wb_conn, "WRITEBACK_PIXEL_FORMATS");
       // for each connector
       // set the crtc
    */
}

fn main() -> anyhow::Result<()> {
    let args = args().run();
    let node = format!("/dev/dri/card{}", args.node);
    let f = File::options()
        .read(true)
        .write(true)
        .open(node)
        .with_context(|| "Couldn't open file")?;

    // This will fail if the device doesn't support writeback.
    let dev = AtomicKmsWbDevice::new(f)?;

    if args.list_connectors {
        println!("Connectors:");
        dev.connectors().for_each(|i| {
            println!("\t{} ({:?})", WbConnector::from(i), i.handle());
        });
        println!("Connected:");
        dev.active_connectors().for_each(|i| {
            println!("\t{} ({:?})", WbConnector::from(i), i.handle());
        });
        println!("Writeback:");
        dev.writeback_connectors(false).for_each(|i| {
            println!("\t{} ({:?})", WbConnector::from(i), i.handle());
        });
        return Ok(());
    }

    let connectors = if args.connectors.is_empty() {
        dev.active_connectors().map(|i| i.into()).collect()
    } else {
        args.connectors
    };

    let connectors: Vec<&connector::Info> = connectors
        .iter()
        .map(|wbc| dev.connector_from_name(wbc).unwrap())
        .collect();

    if dev.authenticated().is_ok_and(|a| a) {
        // We are master
        do_screenshot(&dev, connectors.as_slice());
    } else {
        todo!("I should be a wayland client and get a DRM lease");
    }

    Ok(())
}
