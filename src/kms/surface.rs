use std::{
    collections::HashSet,
    sync::{atomic::AtomicBool, Arc, Mutex, RwLock},
};

use drm::control::{connector, crtc, plane, property, Mode};

use super::AtomicKmsWbDevice;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct State {
    pub mode: Mode,
    pub blob: property::Value<'static>,
    pub connectors: HashSet<connector::Handle>,
}

pub struct AtomicSurface {
    active: Arc<AtomicBool>,
    crtc: crtc::Handle,
    plane: plane::Handle,
    used_planes: Mutex<HashSet<plane::Handle>>,
    state: RwLock<State>,
    pending: RwLock<State>,
}

impl AtomicSurface {
    pub fn new(_dev: AtomicKmsWbDevice) {}
}
